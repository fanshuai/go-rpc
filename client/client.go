package main

import (
	"context"
	"flag"
	"fmt"
	pb "go-programming-tour-book/gorpc/protoc"
	"google.golang.org/grpc"
	"io"
	"log"
)

var port string

func init() {
	flag.StringVar(&port, "p", "8000", "启动端口号")
	flag.Parse()
}

func main() {
	conn, _ := grpc.Dial(":" + port, grpc.WithInsecure())
	defer func() {
		_ = conn.Close()
	}()
	client := pb.NewGreeterClient(conn)
	_ = SayHello(client, "Steve")
	_ = SayHello(client, "Eddy")
	_ = SayList(client, "Tony")
	_ = SayRecord(client, "Jone")
	_ = SayRoute(client, "Wow")
}

func SayHello(client pb.GreeterClient, name string) error {
	resp, err := client.SayHello(context.Background(), &pb.HelloRequest{Name: name})
	if err != nil {
		return err
	}
	log.Printf("client.Sayhello resp: %s", resp.Message)
	return nil
}

func SayList(client pb.GreeterClient, name string) error {
	stream, err := client.SayList(context.Background(), &pb.HelloRequest{Name: name})
	if err != nil {
		return err
	}
	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			log.Printf("client.SayList EOF %v", resp)
			break
		}
		if err != nil {
			return err
		}
		log.Printf("client.SayList resp: %v", resp)
	}
	return nil
}

func SayRecord(client pb.GreeterClient, name string) error {
	stream, err := client.SayRecord(context.Background())
	if err != nil {
		return err
	}
	for n := 0; n <= 6; n++ {
		indexName := fmt.Sprintf("[%d] %s", n, name)
		_ = stream.Send(&pb.HelloRequest{Name: indexName})
	}
	resp, err := stream.CloseAndRecv()
	if err != nil {
		return err
	}
	log.Printf("client.SayRecord resp: %v", resp)
	return nil
}


func SayRoute(client pb.GreeterClient, name string) error {
	stream, err := client.SayRoute(context.Background())
	if err != nil {
		return err
	}
	for n := 0; n <= 6; n++ {
		indexName := fmt.Sprintf("[%d] %s", n, name)
		_ = stream.Send(&pb.HelloRequest{Name: indexName})
		resp, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		log.Printf("client.SayRoute resp: %v", resp)
	}
	_ = stream.CloseSend()
	return nil
}