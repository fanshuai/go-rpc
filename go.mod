module go-programming-tour-book/gorpc

go 1.16

require (
	github.com/golang/protobuf v1.4.3
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
	golang.org/x/sys v0.0.0-20210314195730-07df6a141424 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
