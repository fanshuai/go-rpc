package main

import (
	"context"
	"flag"
	"fmt"
	pb "go-programming-tour-book/gorpc/protoc"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
)

var port string

func init() {
	flag.StringVar(&port, "p", "8000", "启动端口号")
	flag.Parse()
}

type GreeterServer struct {}

func (s *GreeterServer) SayHello(ctx context.Context, r *pb.HelloRequest) (*pb.HelloReply, error) {
	message := fmt.Sprintf("hello.world %s\n", r.Name)
	log.Printf("\nserver.SayHello ctx: %v \nresp:%s\n", ctx, message)
	return &pb.HelloReply{Message: message}, nil
}

func (s *GreeterServer) SayList(r * pb.HelloRequest, stream pb.Greeter_SayListServer) error {
	var message string
	for n := 0; n <= 6; n++ {
		message = fmt.Sprintf("[%d] hello.world %s\n", n+1, r.Name)
		log.Printf("\nserver.SayList resp:%s\n", message)
		_ = stream.Send(&pb.HelloReply{Message: message})
	}
	return nil
}

func (s *GreeterServer) SayRecord(stream pb.Greeter_SayRecordServer) error {
	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			log.Printf("server.SayRecord EOF %v", resp)
			message := &pb.HelloReply{Message: "say.record"}
			return stream.SendAndClose(message)
		}
		if err != nil {
			return err
		}
		log.Printf("\nserver.SayRecord recv:%s\n", resp)
	}
}

func (s *GreeterServer) SayRoute(stream pb.Greeter_SayRouteServer) error {
	n := 0
	var message string
	for {
		message = fmt.Sprintf("say.route %d", n)
		_ = stream.Send(&pb.HelloReply{Message: message})
		resp, err := stream.Recv()
		if err == io.EOF {
			log.Printf("server.SayRoute EOF %v", resp)
			return nil
		}
		if err != nil {
			return err
		}
		n++
		log.Printf("\nserver.SayRoute recv:%s\n", resp)
	}
}

func main() {
	server := grpc.NewServer()
	pb.RegisterGreeterServer(server, &GreeterServer{})
	lis, _ := net.Listen("tcp", ":" + port)
	_ = server.Serve(lis)
}